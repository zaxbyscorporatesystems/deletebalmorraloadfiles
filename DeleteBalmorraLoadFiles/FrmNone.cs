﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Windows.Forms;

namespace DeleteBalmorraLoadFiles
{
    public partial class FrmNone : Form
    {
        public FrmNone()
        {
            InitializeComponent();
        }

        private void FrmNone_Load(object sender, EventArgs e)
        {
            deleteOldFiles(@"\\balmorra\QB650IN\", true);
            //deleteOldFiles(@"\\balmorra\QB685IN\", false); disabled per Amy 8-28-17   AMB
            Application.Exit();
        }
        private void deleteOldFiles(string folDer, bool replaceWithEmptyFile)
        {
            try
            {
                    string[] fyles = Directory.GetFiles(String.Format(@"{0}", folDer), "*.*");
                    foreach (var fyle in fyles)
                    {
                        File.Delete(fyle);
                        if(replaceWithEmptyFile==true)
                        {
                            File.AppendAllText(fyle, string.Empty);     // dcarter 8/22/2016    create empty file  
                        }

                    }
            }
            catch 
            {
                //MessageBox.Show(ex.Message);
            }
            return;
        }

    }
}
